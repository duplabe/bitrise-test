<?php

declare(strict_types=1);

use Bitrise\CommandBus\Command\ChangeAppLimits;
use Bitrise\CommandBus\Command\CreateApp;
use Bitrise\CommandBus\Command\CreatePlan;
use Bitrise\CommandBus\Command\CreateUser;
use Bitrise\CommandBus\Command\OptOutFromPublicAppLimits;
use Bitrise\CommandBus\Handler\ChangeAppLimitsHandler;
use Bitrise\CommandBus\Handler\CreateAppHandler;
use Bitrise\CommandBus\Handler\CreatePlanHandler;
use Bitrise\CommandBus\Handler\CreateUserHandler;
use Bitrise\CommandBus\Handler\OptOutFromPublicAppLimitsHandler;
use Bitrise\Limit\Provider;
use Bitrise\Mock\Storage\InMemoryApp;
use Bitrise\Mock\Storage\InMemoryLimit;
use Bitrise\Mock\Storage\InMemoryPlan;
use Bitrise\Mock\Storage\InMemoryUser;
use Bitrise\Storage\AppInterface;
use Bitrise\Storage\LimitInterface;
use Bitrise\Storage\PlanInterface;
use Bitrise\Storage\UserInterface;
use CommandBus\CommandBus;
use CommandBus\ContainerAwareMiddleware;
use Pimple\Psr11\Container;
use Psr\Container\ContainerInterface;

$container = new \Pimple\Container();

$container[ContainerInterface::class] = static fn ($c) => new Container($c);

$container[CommandBus::class] = static function ($c) {
    $commandBus = new CommandBus();
    $commandBus->addMiddleware($c[ContainerAwareMiddleware::class]);

    return $commandBus;
};

$container[ContainerAwareMiddleware::class] = static fn ($c) => new ContainerAwareMiddleware(
    $c[ContainerInterface::class],
    [
        CreateApp::class => CreateAppHandler::class,
        CreatePlan::class => CreatePlanHandler::class,
        CreateUser::class => CreateUserHandler::class,
        ChangeAppLimits::class => ChangeAppLimitsHandler::class,
        OptOutFromPublicAppLimits::class => OptOutFromPublicAppLimitsHandler::class,
    ]
);

$container[AppInterface::class] = static fn ($c) => new InMemoryApp();
$container[CreateAppHandler::class] = static fn ($c) => new CreateAppHandler($c[AppInterface::class]);

$container[PlanInterface::class] = static fn ($c) => new InMemoryPlan();
$container[CreatePlanHandler::class] = static fn ($c) => new CreatePlanHandler($c[PlanInterface::class]);

$container[UserInterface::class] = static fn ($c) => new InMemoryUser();
$container[CreateUserHandler::class] = static fn ($c) => new CreateUserHandler($c[UserInterface::class]);

$container[LimitInterface::class] = static fn ($c) => new InMemoryLimit();

$container[ChangeAppLimitsHandler::class] = static fn ($c) => new ChangeAppLimitsHandler($c[AppInterface::class], $c[LimitInterface::class]);
$container[OptOutFromPublicAppLimitsHandler::class] = static fn ($c) => new OptOutFromPublicAppLimitsHandler($c[AppInterface::class]);

$container[Provider::class] = static fn ($c) => new Provider($c[UserInterface::class], $c[PlanInterface::class], $c[LimitInterface::class]);

return $container[ContainerInterface::class];
