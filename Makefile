docker-run = docker run --rm -v ${PWD}:/srv bb-bitrise-test $(1)

build:
	docker build . -t bb-bitrise-test

composer-install: build
	$(call docker-run,composer install)

test: composer-install
	$(call docker-run,vendor/bin/phpunit)
