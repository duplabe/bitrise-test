# Bitrise Test Application

It's a really simple implementation of the CQRS pattern. Write operations go through the CommandBus (`src/CommandBus`). 
Each Command (`src/Bitrise/CommandBus/Command`) has its own Handler (`src/Bitrise/CommandBus/Handler`).
For reading one can use the Storages (`src/Bitrise/Storage`) directly.
The business logic of finding the limitations of an application is inside the Provider (`src/Bitrise/Limit/Provider.php`)
(yes, I'm either not sure about the name `Provider`, but it is good for now :)).

## Further improvements

I've made a couple of shortcuts in the implementation:

- it would be a good idea to split the storage interfaces between read and write operations (but it is ok for me to 
implement both in the same classes). In this case it would be even noticeable if a developer uses write operations instead 
of the CommandBus.
- handlers are very simple now and one can think that they are useless, but they are the perfect places for further 
operations like sending emails, updating statistics, etc.
- the middleware pattern seems also useless now, but it allows to add logging, transaction handling, etc. later

## Tests

Running tests inside docker:

`make test`

Coverage report: `build/coverage-report`.
