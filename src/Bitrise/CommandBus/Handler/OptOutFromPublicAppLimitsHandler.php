<?php

declare(strict_types=1);

namespace Bitrise\CommandBus\Handler;

use Bitrise\CommandBus\Command\OptOutFromPublicAppLimits;
use Bitrise\Exception\LogicException;
use Bitrise\Storage\AppInterface;

final class OptOutFromPublicAppLimitsHandler
{
    private AppInterface $appStorage;

    public function __construct(AppInterface $appStorage)
    {
        $this->appStorage = $appStorage;
    }

    public function __invoke(OptOutFromPublicAppLimits $changeAppLimits)
    {
        $app = $this->appStorage->get($changeAppLimits->getAppId());

        if ($app->isPrivate()) {
            throw new LogicException();
        }

        $app->optOutFromPublicLimits();

        $this->appStorage->add($app);
    }
}
