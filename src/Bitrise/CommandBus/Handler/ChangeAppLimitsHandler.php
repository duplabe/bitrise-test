<?php

declare(strict_types=1);

namespace Bitrise\CommandBus\Handler;

use Bitrise\CommandBus\Command\ChangeAppLimits;
use Bitrise\DTO\Limit;
use Bitrise\Exception\LogicException;
use Bitrise\Storage\AppInterface;
use Bitrise\Storage\LimitInterface;

final class ChangeAppLimitsHandler
{
    private AppInterface $appStorage;
    private LimitInterface $limitStorage;

    public function __construct(AppInterface $appStorage, LimitInterface $limitStorage)
    {
        $this->appStorage = $appStorage;
        $this->limitStorage = $limitStorage;
    }

    public function __invoke(ChangeAppLimits $changeAppLimits)
    {
        $app = $this->appStorage->get($changeAppLimits->getAppId());

        if ($app->isPrivate()) {
            throw new LogicException();
        }

        $this->limitStorage->add(new Limit($changeAppLimits->getAppId(), $changeAppLimits->getLimits()));
    }
}
