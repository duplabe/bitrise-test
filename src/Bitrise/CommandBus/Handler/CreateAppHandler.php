<?php

declare(strict_types=1);

namespace Bitrise\CommandBus\Handler;

use Bitrise\CommandBus\Command\CreateApp;
use Bitrise\DTO\App;
use Bitrise\Storage\AppInterface;

final class CreateAppHandler
{
    private AppInterface $storage;

    public function __construct(AppInterface $storage)
    {
        $this->storage = $storage;
    }

    public function __invoke(CreateApp $createApp)
    {
        $this->storage->add(new App(
            $createApp->getId(),
            $createApp->getName(),
            $createApp->getOwner(),
            $createApp->getVisibility(),
        ));
    }
}
