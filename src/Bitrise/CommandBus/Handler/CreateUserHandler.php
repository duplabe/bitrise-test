<?php

declare(strict_types=1);

namespace Bitrise\CommandBus\Handler;

use Bitrise\CommandBus\Command\CreateUser;
use Bitrise\DTO\User;
use Bitrise\Storage\UserInterface;

final class CreateUserHandler
{
    private UserInterface $storage;

    public function __construct(UserInterface $storage)
    {
        $this->storage = $storage;
    }

    public function __invoke(CreateUser $createUser)
    {
        $this->storage->add(new User(
            $createUser->getId(),
            $createUser->getName(),
            $createUser->getEmail(),
            $createUser->getPlan()
        ));
    }
}
