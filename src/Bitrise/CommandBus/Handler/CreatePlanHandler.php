<?php

declare(strict_types=1);

namespace Bitrise\CommandBus\Handler;

use Bitrise\CommandBus\Command\CreatePlan;
use Bitrise\DTO\Plan;
use Bitrise\Storage\PlanInterface;

final class CreatePlanHandler
{
    private PlanInterface $storage;

    public function __construct(PlanInterface $storage)
    {
        $this->storage = $storage;
    }

    public function __invoke(CreatePlan $createPlan)
    {
        $this->storage->add(new Plan(
            $createPlan->getId(),
            $createPlan->getName(),
            $createPlan->getLimits()
        ));
    }
}
