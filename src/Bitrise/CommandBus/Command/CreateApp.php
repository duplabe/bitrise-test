<?php

declare(strict_types=1);

namespace Bitrise\CommandBus\Command;

use Bitrise\DTO\Value\AppId;
use Bitrise\DTO\Value\UserId;

final class CreateApp
{
    private AppId $id;
    private string $name;
    private UserId $owner;
    private string $visibility;

    public function __construct(AppId $id, string $name, UserId $owner, string $visibility)
    {
        $this->id = $id;
        $this->name = $name;
        $this->owner = $owner;
        $this->visibility = $visibility;
    }

    public function getId(): AppId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getOwner(): UserId
    {
        return $this->owner;
    }

    public function getVisibility(): string
    {
        return $this->visibility;
    }
}
