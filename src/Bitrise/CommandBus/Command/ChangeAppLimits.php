<?php

declare(strict_types=1);

namespace Bitrise\CommandBus\Command;

use Bitrise\DTO\Value\AppId;
use Bitrise\DTO\Value\Limits;

final class ChangeAppLimits
{
    private AppId $appId;
    private Limits $limits;

    public function __construct(AppId $appId, Limits $limits)
    {
        $this->appId = $appId;
        $this->limits = $limits;
    }

    public function getAppId(): AppId
    {
        return $this->appId;
    }

    public function getLimits(): Limits
    {
        return $this->limits;
    }
}
