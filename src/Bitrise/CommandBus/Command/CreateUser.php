<?php

declare(strict_types=1);

namespace Bitrise\CommandBus\Command;

use Bitrise\DTO\Value\PlanId;
use Bitrise\DTO\Value\UserId;

final class CreateUser
{
    private UserId $id;
    private PlanId $plan;
    private string $name;
    private string $email;

    public function __construct(UserId $id, PlanId $plan, string $name, string $email)
    {
        $this->id = $id;
        $this->plan = $plan;
        $this->name = $name;
        $this->email = $email;
    }

    public function getId(): UserId
    {
        return $this->id;
    }

    public function getPlan(): PlanId
    {
        return $this->plan;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
