<?php

declare(strict_types=1);

namespace Bitrise\CommandBus\Command;

use Bitrise\DTO\Value\Limits;
use Bitrise\DTO\Value\PlanId;

final class CreatePlan
{
    private PlanId $id;
    private string $name;
    private Limits $limits;

    public function __construct(PlanId $id, string $name, Limits $limits)
    {
        $this->id = $id;
        $this->name = $name;
        $this->limits = $limits;
    }

    public function getId(): PlanId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLimits(): Limits
    {
        return $this->limits;
    }
}
