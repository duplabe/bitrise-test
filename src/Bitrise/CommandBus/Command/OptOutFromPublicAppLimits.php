<?php

declare(strict_types=1);

namespace Bitrise\CommandBus\Command;

use Bitrise\DTO\Value\AppId;

final class OptOutFromPublicAppLimits
{
    private AppId $appId;

    public function __construct(AppId $appId)
    {
        $this->appId = $appId;
    }

    public function getAppId(): AppId
    {
        return $this->appId;
    }
}
