<?php

declare(strict_types=1);

namespace Bitrise\DTO;

use Bitrise\DTO\Value\Limits;
use Bitrise\DTO\Value\PlanId;
use Bitrise\Util\Assert;

final class Plan
{
    private PlanId $id;
    private string $name;
    private Limits $limits;

    public function __construct(PlanId $id, string $name, Limits $limits)
    {
        Assert::notEmpty($name);

        $this->id = $id;
        $this->name = $name;
        $this->limits = $limits;
    }

    public function getId(): PlanId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLimits(): Limits
    {
        return $this->limits;
    }
}
