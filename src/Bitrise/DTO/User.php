<?php

declare(strict_types=1);

namespace Bitrise\DTO;

use Bitrise\DTO\Value\PlanId;
use Bitrise\DTO\Value\UserId;
use Bitrise\Util\Assert;

final class User
{
    private UserId $id;
    private string $name;
    private string $email;
    private PlanId $plan;

    public function __construct(UserId $id, string $name, string $email, PlanId $plan)
    {
        Assert::notEmpty($name);
        Assert::email($email);

        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->plan = $plan;
    }

    public function getId(): UserId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPlan(): PlanId
    {
        return $this->plan;
    }
}
