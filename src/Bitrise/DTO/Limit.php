<?php

declare(strict_types=1);

namespace Bitrise\DTO;

use Bitrise\DTO\Value\AppId;
use Bitrise\DTO\Value\Limits;

final class Limit
{
    private AppId $id;
    private Limits $limits;

    public function __construct(AppId $id, Limits $limits)
    {
        $this->id = $id;
        $this->limits = $limits;
    }

    public function getId(): AppId
    {
        return $this->id;
    }

    public function getLimits(): Limits
    {
        return $this->limits;
    }
}
