<?php

declare(strict_types=1);

namespace Bitrise\DTO;

use Bitrise\DTO\Value\AppId;
use Bitrise\DTO\Value\UserId;
use Bitrise\Util\Assert;

final class App
{
    public const VISIBILITY_PUBLIC = 'public';
    public const VISIBILITY_PRIVATE = 'private';

    private AppId $id;
    private string $name;
    private UserId $owner;
    private string $visibility;
    private bool $optedOutFromPublicLimits = false;

    public function __construct(AppId $id, string $name, UserId $owner, string $visibility)
    {
        Assert::notEmpty($name);
        Assert::inArray($visibility, [self::VISIBILITY_PRIVATE, self::VISIBILITY_PUBLIC]);

        $this->id = $id;
        $this->name = $name;
        $this->owner = $owner;
        $this->visibility = $visibility;
    }

    public function getId(): AppId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getOwner(): UserId
    {
        return $this->owner;
    }

    public function isPrivate(): bool
    {
        return self::VISIBILITY_PRIVATE === $this->visibility;
    }

    public function optOutFromPublicLimits(): void
    {
        $this->optedOutFromPublicLimits = true;
    }

    public function isOptedOutFromPublicLimits(): bool
    {
        return $this->optedOutFromPublicLimits;
    }
}
