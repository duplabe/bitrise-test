<?php

declare(strict_types=1);

namespace Bitrise\DTO\Value;

use Bitrise\Util\Assert;

abstract class Id
{
    private string $id;

    protected function __construct(string $id)
    {
        Assert::notEmpty($id);

        $this->id = $id;
    }

    /**
     * @return static
     */
    public static function createFromString(string $id): self
    {
        return new static($id);
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
