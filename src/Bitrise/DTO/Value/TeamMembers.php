<?php

declare(strict_types=1);

namespace Bitrise\DTO\Value;

use Bitrise\Util\Assert;

final class TeamMembers
{
    private bool $unlimited;
    private int $count = 0;

    public function __construct(bool $unlimited, int $count = 0)
    {
        $this->unlimited = $unlimited;
        if (!$unlimited) {
            Assert::greaterThan($count, 0);
            $this->count = $count;
        }
    }

    public function isUnlimited(): bool
    {
        return $this->unlimited;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}
