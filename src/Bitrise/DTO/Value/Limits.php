<?php

declare(strict_types=1);

namespace Bitrise\DTO\Value;

use Bitrise\Util\Assert;

final class Limits
{
    private int $concurrentBuilds;
    private int $buildTime;
    private BuildsPerMonth $buildsPerMonth;
    private TeamMembers $teamMembers;

    public function __construct(int $concurrentBuilds, int $buildTime, BuildsPerMonth $buildsPerMonth, TeamMembers $teamMembers)
    {
        Assert::greaterThan($concurrentBuilds, 0);
        Assert::greaterThan($buildTime, 0);

        $this->concurrentBuilds = $concurrentBuilds;
        $this->buildTime = $buildTime;
        $this->buildsPerMonth = $buildsPerMonth;
        $this->teamMembers = $teamMembers;
    }

    public function getConcurrentBuilds(): int
    {
        return $this->concurrentBuilds;
    }

    public function getBuildTime(): int
    {
        return $this->buildTime;
    }

    public function getBuildsPerMonth(): BuildsPerMonth
    {
        return $this->buildsPerMonth;
    }

    public function getTeamMembers(): TeamMembers
    {
        return $this->teamMembers;
    }
}
