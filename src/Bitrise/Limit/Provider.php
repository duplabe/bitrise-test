<?php

declare(strict_types=1);

namespace Bitrise\Limit;

use Bitrise\DTO\App;
use Bitrise\DTO\Value\BuildsPerMonth;
use Bitrise\DTO\Value\Limits;
use Bitrise\DTO\Value\TeamMembers;
use Bitrise\Storage\LimitInterface;
use Bitrise\Storage\PlanInterface;
use Bitrise\Storage\UserInterface;

final class Provider
{
    private UserInterface $userStorage;
    private PlanInterface $planStorage;
    private LimitInterface $limitStorage;

    public function __construct(UserInterface $userStorage, PlanInterface $planStorage, LimitInterface $limitStorage)
    {
        $this->userStorage = $userStorage;
        $this->planStorage = $planStorage;
        $this->limitStorage = $limitStorage;
    }

    public function getLimits(App $app): Limits
    {
        // private and opted out apps inherit the limits form the owner's plan
        if ($app->isPrivate() || $app->isOptedOutFromPublicLimits()) {
            return $this->planStorage->get($this->userStorage->get($app->getOwner())->getPlan())->getLimits();
        }

        // check the public app has custom limits
        if ($this->limitStorage->has($app->getId())) {
            return $this->limitStorage->get($app->getId())->getLimits();
        }

        // otherwise get the public app limits
        return $this->getPublicAppLimits();
    }

    public function getPublicAppLimits(): Limits
    {
        return new Limits(2, 45, new BuildsPerMonth(true), new TeamMembers(true));
    }
}
