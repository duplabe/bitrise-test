<?php

declare(strict_types=1);

namespace Bitrise\Exception;

final class LogicException extends \Exception implements Exception
{
}
