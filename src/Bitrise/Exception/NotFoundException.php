<?php

declare(strict_types=1);

namespace Bitrise\Exception;

final class NotFoundException extends \Exception implements Exception
{
}
