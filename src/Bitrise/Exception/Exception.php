<?php

declare(strict_types=1);

namespace Bitrise\Exception;

/**
 * Marker interface for domain specific exceptions.
 */
interface Exception
{
}
