<?php

declare(strict_types=1);

namespace Bitrise\Exception;

final class InvalidArgumentException extends \Exception implements Exception
{
}
