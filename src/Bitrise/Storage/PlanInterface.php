<?php

declare(strict_types=1);

namespace Bitrise\Storage;

use Bitrise\DTO\Plan;
use Bitrise\DTO\Value\PlanId;
use Bitrise\Exception\NotFoundException;

interface PlanInterface
{
    public function add(Plan $plan): void;

    /**
     * @throws NotFoundException
     */
    public function get(PlanId $id): Plan;
}
