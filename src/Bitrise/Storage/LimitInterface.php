<?php

declare(strict_types=1);

namespace Bitrise\Storage;

use Bitrise\DTO\Limit;
use Bitrise\DTO\Value\AppId;
use Bitrise\Exception\NotFoundException;

interface LimitInterface
{
    public function add(Limit $limit): void;

    public function has(AppId $id): bool;

    /**
     * @throws NotFoundException
     */
    public function get(AppId $appId): Limit;
}
