<?php

declare(strict_types=1);

namespace Bitrise\Storage;

use Bitrise\DTO\App;
use Bitrise\DTO\Value\AppId;
use Bitrise\Exception\NotFoundException;

interface AppInterface
{
    public function add(App $app): void;

    /**
     * @throws NotFoundException
     */
    public function get(AppId $appId): App;
}
