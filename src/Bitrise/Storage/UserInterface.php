<?php

declare(strict_types=1);

namespace Bitrise\Storage;

use Bitrise\DTO\User;
use Bitrise\DTO\Value\UserId;
use Bitrise\Exception\NotFoundException;

interface UserInterface
{
    public function add(User $user): void;

    /**
     * @throws NotFoundException
     */
    public function get(UserId $id): User;
}
