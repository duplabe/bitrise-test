<?php

declare(strict_types=1);

namespace CommandBus;

interface Middleware
{
    public function handle(object $command, callable $next): void;
}
