<?php

declare(strict_types=1);

namespace CommandBus;

use Psr\Container\ContainerInterface;
use Webmozart\Assert\Assert;

final class ContainerAwareMiddleware implements Middleware
{
    private ContainerInterface $container;

    /**
     * @var string[]
     */
    private array $handlerMap;

    /**
     * @param string[] $handlerMap
     */
    public function __construct(ContainerInterface $container, array $handlerMap = [])
    {
        Assert::allString(\array_keys($handlerMap));
        Assert::allString($handlerMap);

        $this->container = $container;
        $this->handlerMap = $handlerMap;
    }

    public function handle(object $command, callable $next): void
    {
        $this->findHandlerForCommand($command)($command);

        $next($command);
    }

    private function findHandlerForCommand(object $command): callable
    {
        if (isset($this->handlerMap[\get_class($command)])) {
            $handler = $this->container->get($this->handlerMap[\get_class($command)]);

            Assert::isCallable($handler);

            return $handler;
        }

        return function () {};
    }
}
