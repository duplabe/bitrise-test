<?php

declare(strict_types=1);

namespace CommandBus;

final class CommandBus
{
    /**
     * @var Middleware[]
     */
    private array $middlewares = [];

    public function addMiddleware(Middleware $middleware): void
    {
        $this->middlewares[] = $middleware;
    }

    public function handle(object $command): void
    {
        $this->callableForMiddleWare(0)($command);
    }

    private function callableForMiddleWare(int $index): callable
    {
        if (!isset($this->middlewares[$index])) {
            return function (object $command): void {};
        }

        return function (object $command) use ($index): void {
            $this->middlewares[$index]->handle($command, $this->callableForMiddleWare($index + 1));
        };
    }
}
