<?php

declare(strict_types=1);

namespace Bitrise\Mock\Storage;

use Bitrise\DTO\App;
use Bitrise\DTO\Value\AppId;
use Bitrise\Exception\NotFoundException;
use Bitrise\Storage\AppInterface;
use Ramsey\Uuid\Uuid;

class InMemoryApp implements AppInterface
{
    /**
     * @var App[]
     */
    private array $storage = [];

    public function add(App $app): void
    {
        $this->storage[(string) $app->getId()] = $app;
    }

    public function nextId(): AppId
    {
        return AppId::createFromString((string) Uuid::uuid4());
    }

    public function has(AppId $id): bool
    {
        return \array_key_exists((string) $id, $this->storage);
    }

    public function get(AppId $id): App
    {
        if ($this->has($id)) {
            return $this->storage[(string) $id];
        }

        throw new NotFoundException();
    }
}
