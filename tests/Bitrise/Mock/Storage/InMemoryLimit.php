<?php

declare(strict_types=1);

namespace Bitrise\Mock\Storage;

use Bitrise\DTO\Limit;
use Bitrise\DTO\Value\AppId;
use Bitrise\Exception\NotFoundException;
use Bitrise\Storage\LimitInterface;

class InMemoryLimit implements LimitInterface
{
    /**
     * @var Limit[]
     */
    private array $storage = [];

    public function add(Limit $limit): void
    {
        $this->storage[(string) $limit->getId()] = $limit;
    }

    public function has(AppId $id): bool
    {
        return \array_key_exists((string) $id, $this->storage);
    }

    public function get(AppId $id): Limit
    {
        if ($this->has($id)) {
            return $this->storage[(string) $id];
        }

        throw new NotFoundException();
    }
}
