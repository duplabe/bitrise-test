<?php

declare(strict_types=1);

namespace Bitrise\Mock\Storage;

use Bitrise\DTO\Plan;
use Bitrise\DTO\Value\PlanId;
use Bitrise\Exception\NotFoundException;
use Bitrise\Storage\PlanInterface;
use Ramsey\Uuid\Uuid;

class InMemoryPlan implements PlanInterface
{
    /**
     * @var Plan[]
     */
    private array $storage = [];

    public function add(Plan $plan): void
    {
        $this->storage[(string) $plan->getId()] = $plan;
    }

    public function nextId(): PlanId
    {
        return PlanId::createFromString((string) Uuid::uuid4());
    }

    public function has(PlanId $id): bool
    {
        return \array_key_exists((string) $id, $this->storage);
    }

    public function get(PlanId $id): Plan
    {
        if ($this->has($id)) {
            return $this->storage[(string) $id];
        }

        throw new NotFoundException();
    }
}
