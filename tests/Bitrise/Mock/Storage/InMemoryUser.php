<?php

declare(strict_types=1);

namespace Bitrise\Mock\Storage;

use Bitrise\DTO\User;
use Bitrise\DTO\Value\UserId;
use Bitrise\Exception\NotFoundException;
use Bitrise\Storage\UserInterface;
use Ramsey\Uuid\Uuid;

class InMemoryUser implements UserInterface
{
    /**
     * @var User[]
     */
    private array $storage = [];

    public function add(User $user): void
    {
        $this->storage[(string) $user->getId()] = $user;
    }

    public function nextId(): UserId
    {
        return UserId::createFromString((string) Uuid::uuid4());
    }

    public function has(UserId $id): bool
    {
        return \array_key_exists((string) $id, $this->storage);
    }

    public function get(UserId $id): User
    {
        if ($this->has($id)) {
            return $this->storage[(string) $id];
        }

        throw new NotFoundException();
    }
}
