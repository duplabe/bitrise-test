<?php

declare(strict_types=1);

namespace Bitrise\Test;

use Bitrise\DTO\App;
use Bitrise\DTO\Plan;
use Bitrise\DTO\User;
use Bitrise\DTO\Value\AppId;
use Bitrise\DTO\Value\BuildsPerMonth;
use Bitrise\DTO\Value\Limits;
use Bitrise\DTO\Value\PlanId;
use Bitrise\DTO\Value\TeamMembers;
use Bitrise\DTO\Value\UserId;
use Bitrise\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class AssertTest extends TestCase
{
    public function testEmptyCreateAppName(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new App(AppId::createFromString('1'), '', UserId::createFromString('1'), App::VISIBILITY_PUBLIC);
    }

    public function testInvalidCreateAppVisibility(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new App(AppId::createFromString('1'), 'test', UserId::createFromString('1'), '');
    }

    public function testEmptyCreatePlanName(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new Plan(PlanId::createFromString('1'), '', new Limits(1, 1, new BuildsPerMonth(true), new TeamMembers(true)));
    }

    public function testEmptyCreateUserName(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new User(UserId::createFromString('1'), '', 'test@test.com', PlanId::createFromString('1'));
    }

    public function testInvalidCreateUserEmail(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new User(UserId::createFromString('1'), 'foo', 'testtest.com', PlanId::createFromString('1'));
    }

    public function testInvalidLimitsConcurrentBuilds(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new Limits(0, 1, new BuildsPerMonth(true), new TeamMembers(true));
    }

    public function testInvalidLimitsBuildTime(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new Limits(1, 0, new BuildsPerMonth(true), new TeamMembers(true));
    }

    public function testInvalidBuildsPerMonth(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new BuildsPerMonth(false);
    }

    public function testInvalidTeamMembers(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new TeamMembers(false);
    }
}
