<?php

declare(strict_types=1);

namespace Bitrise\Test;

use Bitrise\CommandBus\Command\ChangeAppLimits;
use Bitrise\CommandBus\Command\CreateApp;
use Bitrise\CommandBus\Command\CreatePlan;
use Bitrise\CommandBus\Command\CreateUser;
use Bitrise\CommandBus\Command\OptOutFromPublicAppLimits;
use Bitrise\DTO\App;
use Bitrise\DTO\User;
use Bitrise\DTO\Value\BuildsPerMonth;
use Bitrise\DTO\Value\Limits;
use Bitrise\DTO\Value\PlanId;
use Bitrise\DTO\Value\TeamMembers;
use Bitrise\DTO\Value\UserId;
use Bitrise\Exception\LogicException;
use Bitrise\Limit\Provider;
use Bitrise\Storage\AppInterface;
use Bitrise\Storage\PlanInterface;
use Bitrise\Storage\UserInterface;
use CommandBus\CommandBus;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class IntegrationTest extends TestCase
{
    private ContainerInterface $container;
    private PlanId $planId;

    protected function setUp(): void
    {
        $container = require __DIR__.'/../../config/container.php';
        \assert($container instanceof ContainerInterface);
        $this->container = $container;

        $this->prepareDefaultPlans();
    }

    public function testCreatePrivateApp(): void
    {
        // create a new user
        $user = $this->setUpANewUser();

        // create a private app for that user
        $app = $this->setUpAApp($user->getId(), App::VISIBILITY_PRIVATE);

        // get the limits of the app
        $this->assertLimits($this->getPlanStorage()->get($user->getPlan())->getLimits(), $this->getLimitProvider()->getLimits($app));
    }

    public function testCreatePublicApp(): void
    {
        // create a new user
        $user = $this->setUpANewUser();

        // create an app for that user
        $app = $this->setUpAApp($user->getId(), App::VISIBILITY_PUBLIC);

        // get the limits of the app
        $this->assertLimits($this->getLimitProvider()->getPublicAppLimits(), $this->getLimitProvider()->getLimits($app));
    }

    public function testChangeAppLimitsForPublicApp(): void
    {
        // create a new user
        $user = $this->setUpANewUser();

        // create an app for that user
        $app = $this->setUpAApp($user->getId(), App::VISIBILITY_PUBLIC);

        // change the limits for that app
        $limits = new Limits(5, 600, new BuildsPerMonth(false, 1000), new TeamMembers(false, 10));
        $this->getCommandBus()->handle(new ChangeAppLimits($app->getId(), $limits));

        // get the limits of the app
        $this->assertLimits($limits, $this->getLimitProvider()->getLimits($app));
    }

    public function testChangeAppLimitsForPrivateApp(): void
    {
        // create a new user
        $user = $this->setUpANewUser();

        // create an app for that user
        $app = $this->setUpAApp($user->getId(), App::VISIBILITY_PRIVATE);

        // change the limits for that app
        $limits = new Limits(5, 600, new BuildsPerMonth(false, 1000), new TeamMembers(false, 10));

        $this->expectException(LogicException::class);

        $this->getCommandBus()->handle(new ChangeAppLimits($app->getId(), $limits));
    }

    public function testOptOutPublicApp(): void
    {
        // create a new user
        $user = $this->setUpANewUser();

        // create a public app for that user
        $app = $this->setUpAApp($user->getId(), App::VISIBILITY_PUBLIC);

        // opt out from public limits
        $this->getCommandBus()->handle(new OptOutFromPublicAppLimits($app->getId()));

        // get the limits of the app
        $this->assertLimits($this->getPlanStorage()->get($user->getPlan())->getLimits(), $this->getLimitProvider()->getLimits($app));
    }

    public function testOptOutPrivateApp(): void
    {
        // create a new user
        $user = $this->setUpANewUser();

        // create an app for that user
        $app = $this->setUpAApp($user->getId(), App::VISIBILITY_PRIVATE);

        // opt out from public limits
        $this->expectException(LogicException::class);

        $this->getCommandBus()->handle(new OptOutFromPublicAppLimits($app->getId()));
    }

    private function assertLimits(Limits $expected, Limits $actual): void
    {
        $this->assertEquals($expected->getConcurrentBuilds(), $actual->getConcurrentBuilds());
        $this->assertEquals($expected->getBuildTime(), $actual->getBuildTime());

        $this->assertEquals($expected->getBuildsPerMonth(), $actual->getBuildsPerMonth());
        $this->assertEquals($expected->getBuildsPerMonth()->isUnlimited(), $actual->getBuildsPerMonth()->isUnlimited());
        $this->assertEquals($expected->getBuildsPerMonth()->getCount(), $actual->getBuildsPerMonth()->getCount());

        $this->assertEquals($expected->getTeamMembers(), $actual->getTeamMembers());
        $this->assertEquals($expected->getTeamMembers()->isUnlimited(), $actual->getTeamMembers()->isUnlimited());
        $this->assertEquals($expected->getTeamMembers()->getCount(), $actual->getTeamMembers()->getCount());
    }

    private function setUpANewUser(): User
    {
        $userId = $this->getUserStorage()->nextId();
        $this->getCommandBus()->handle(new CreateUser($userId, $this->planId, 'test 1', 'john1@doe.com'));
        $this->assertTrue($this->getUserStorage()->has($userId));
        $user = $this->getUserStorage()->get($userId);
        $this->assertEquals($userId, $user->getId());
        $this->assertNotEmpty($user->getName());
        $this->assertNotEmpty($user->getEmail());

        return $user;
    }

    private function setUpAApp(UserId $userId, string $visibility): App
    {
        $appId = $this->getAppStorage()->nextId();
        $this->getCommandBus()->handle(new CreateApp($appId, 'Test 1', $userId, $visibility));
        $this->assertTrue($this->getAppStorage()->has($appId));
        $app = $this->getAppStorage()->get($appId);
        $this->assertEquals($appId, $app->getId());
        $this->assertNotEmpty($app->getName());

        return $app;
    }

    private function prepareDefaultPlans(): void
    {
        $this->getCommandBus()->handle(new CreatePlan(
            $this->planId = $this->getPlanStorage()->nextId(),
            'Free',
            new Limits(1, 10, new BuildsPerMonth(false, 200), new TeamMembers(false, 2))
        ));

        $this->getPlanStorage()->has($this->planId);
        $this->assertNotEmpty($this->getPlanStorage()->get($this->planId)->getName());
        $this->assertNotEmpty($this->getPlanStorage()->get($this->planId)->getLimits());
    }

    private function getAppStorage(): AppInterface
    {
        return $this->container->get(AppInterface::class);
    }

    private function getPlanStorage(): PlanInterface
    {
        return $this->container->get(PlanInterface::class);
    }

    private function getUserStorage(): UserInterface
    {
        return $this->container->get(UserInterface::class);
    }

    private function getCommandBus(): CommandBus
    {
        return $this->container->get(CommandBus::class);
    }

    private function getLimitProvider(): Provider
    {
        return $this->container->get(Provider::class);
    }
}
