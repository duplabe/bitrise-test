<?php

declare(strict_types=1);

$finder = PhpCsFixer\Finder::create()
	->in(__DIR__ . '/config')
	->in(__DIR__ . '/src')
	->in(__DIR__ . '/tests');

return PhpCsFixer\Config::create()
	->setRiskyAllowed(true)
	->setRules([
		'@Symfony'                    => true,
		'@Symfony:risky'              => true,
		'array_syntax'                => ['syntax' => 'short'],
		'blank_line_before_statement' => true,
		'declare_strict_types'        => true,
		'native_function_invocation'  => true,
		'ordered_imports'             => true,
		'strict_comparison'           => true,
		'strict_param'                => true,
	])
	->setFinder($finder);
