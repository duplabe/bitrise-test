FROM php:7.4-cli-alpine

RUN apk --no-cache update && apk --no-cache add pcre-dev ${PHPIZE_DEPS}

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#RUN pecl install ast && docker-php-ext-enable ast

RUN pecl install xdebug && docker-php-ext-enable xdebug

RUN rm -rf /var/cache/apk/*

VOLUME [ "/srv" ]
WORKDIR "/srv"
